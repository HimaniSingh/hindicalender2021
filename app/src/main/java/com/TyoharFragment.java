package com;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hindi.calander.panchang.R;

public class TyoharFragment extends Fragment {
    RecyclerView recycler;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_tyohar, container, false);
        recycler=view.findViewById(R.id.recycler);

        MyListData[] myListData = new MyListData[] {
                new MyListData("जनवरी","https://smarteduguide.com/wp-content/uploads/2020/11/january-2021-768x967.jpg"),
                new MyListData("फ़रवरी","https://smarteduguide.com/wp-content/uploads/2020/11/February-2021-768x979.jpg"),
                new MyListData("मार्च","https://smarteduguide.com/wp-content/uploads/2020/11/March-2021-768x976.jpg"),
                new MyListData("अप्रैल","https://smarteduguide.com/wp-content/uploads/2020/11/April-2021-768x976.jpg"),
                new MyListData("मई","https://smarteduguide.com/wp-content/uploads/2020/11/May-2021-768x976.jpg"),
                new MyListData("जून","https://smarteduguide.com/wp-content/uploads/2020/11/June-2021-768x976.jpg"),
                new MyListData("जुलाई","https://smarteduguide.com/wp-content/uploads/2020/11/July-2021-768x976.jpg"),
                new MyListData("अगस्त","https://smarteduguide.com/wp-content/uploads/2020/11/August-2021-768x976.jpg"),
                new MyListData("सितंबर","https://smarteduguide.com/wp-content/uploads/2020/11/September-2021-768x976.jpg"),
                new MyListData("अक्टूबर","https://smarteduguide.com/wp-content/uploads/2020/11/October-2021-768x976.jpg"),
                new MyListData("नवंबर","https://smarteduguide.com/wp-content/uploads/2020/11/November-2021-768x976.jpg"),
                new MyListData("दिसंबर","https://smarteduguide.com/wp-content/uploads/2020/11/Decemberr-2021-768x976.jpg")
        };

        MonthsCalenderAdpter adapter = new MonthsCalenderAdpter(getContext(),myListData);
        recycler.setHasFixedSize(true);
        // recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recycler.setLayoutManager(new GridLayoutManager(getContext(),3));
        recycler.setAdapter(adapter);
        return  view;
    }
}