package com;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;

import com.hindi.calander.panchang.R;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class CalenderActivity extends AppCompatActivity {

    ImageView images;
    WebView webView;
    ActionBar actionBar;
    Toolbar toolbar;
    ImageView imageView;
    TextView resulationtext;
    ImageView getImages;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calender);

        webView=findViewById(R.id.help_webview);
       toolbar=findViewById(R.id.too);
        getImages=findViewById(R.id.image);
        resulationtext=toolbar.findViewById(R.id.resulationtext);
       // resulationtext.setVisibility(View.GONE);
        resulationtext.setText(R.string.hindi);
        AddsClass addsClass=new AddsClass(getApplicationContext());
        addsClass.showInterstitial();


        // toolbar.setTitle("िंदी कैलेंडर 2020");
      // setSupportActionBar(toolbar);

       imageView=findViewById(R.id.back);
       imageView.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               finish();
           }
       });



        String url=getIntent().getStringExtra("january");
        //int image=getIntent().getIntExtra("imag",0);
//        getImages.setVisibility(View.VISIBLE);
       // webView.setVisibility(View.GONE);
       // getImages.setImageResource(image);



//        webView.getSettings().setJavaScriptEnabled(true);
//        webView.getSettings().setJavaScriptEnabled(true);


      WebSettings webSettings = webView.getSettings();
      // webSettings.setJavaScriptEnabled(true);
        webView.setInitialScale(1);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
        webSettings.setBuiltInZoomControls(true);
       // webSettings.getDisplayZoomControls();
        webView.findFocus();
        webSettings.getMinimumFontSize();
        webView.requestFocusFromTouch();

       webView.setWebViewClient(new WebViewClient());
        webView.setWebChromeClient(new WebChromeClient());
        webView.loadUrl(url);
       // images.setImageResource(R.drawable.image);

//        URL url = null;
//        try {
//            url = new URL("https://hindijaankaari.in/wp-content/uploads/2018/12/Thakur-prasad-calendar-2020-august.jpg");
//        } catch (MalformedURLException e) {
//            e.printStackTrace();
//        }
//        Bitmap bmp = null;
//        try {
//            bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        images.setImageBitmap(bmp);


    }


}