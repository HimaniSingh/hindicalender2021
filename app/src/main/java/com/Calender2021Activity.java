package com;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.hindi.calander.panchang.R;

public class Calender2021Activity extends AppCompatActivity {
    ImageView getImages;
    WebView webView;
    ActionBar actionBar;
    Toolbar toolbar;
    ImageView imageView;
    TextView resulationtext;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calender2021);
        webView=findViewById(R.id.help_webview);
        toolbar=findViewById(R.id.toolbar);
       // getImages=findViewById(R.id.image);
        resulationtext=toolbar.findViewById(R.id.resulationtext);
        // resulationtext.setVisibility(View.GONE);
        resulationtext.setText(R.string.app_name);
        AddsClass addsClass=new AddsClass(getApplicationContext());
        addsClass.showInterstitial();


        // toolbar.setTitle("िंदी कैलेंडर 2020");
        // setSupportActionBar(toolbar);

        imageView=findViewById(R.id.back);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });



        String url=getIntent().getStringExtra("january");

        // webView.setVisibility(View.GONE);
        // getImages.setImageResource(image);



//        webView.getSettings().setJavaScriptEnabled(true);
//        webView.getSettings().setJavaScriptEnabled(true);


        WebSettings webSettings = webView.getSettings();
        // webSettings.setJavaScriptEnabled(true);
        webView.setInitialScale(1);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
        webSettings.setBuiltInZoomControls(true);
        // webSettings.getDisplayZoomControls();
        webView.findFocus();
        webSettings.getMinimumFontSize();
        webView.requestFocusFromTouch();

        webView.setWebViewClient(new WebViewClient());
        webView.setWebChromeClient(new WebChromeClient());
        webView.loadUrl(url);

    }
}