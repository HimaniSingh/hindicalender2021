package com;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.hindi.calander.panchang.MainActivity;

import java.util.ArrayList;
import java.util.List;

public class ViewPagerAdpter  extends FragmentPagerAdapter {
    Context context;
    int totalTabs;
    public ViewPagerAdpter(Context c, FragmentManager fm, int totalTabs) {
        super(fm);
        context = c;
        this.totalTabs = totalTabs;
    }
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                HomeFragment footballFragment = new HomeFragment();
                return footballFragment;
            case 1:
                TyoharFragment cricketFragment = new TyoharFragment();
                return cricketFragment;

            case 2:
                RashiFragment rashiFragment=new RashiFragment();
                return  rashiFragment;


            default:
                return null;
        }
    }
    @Override
    public int getCount() {
        return totalTabs;
    }
}

