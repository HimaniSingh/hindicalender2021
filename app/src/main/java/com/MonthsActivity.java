package com;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;

import com.hindi.calander.panchang.R;

import java.util.ArrayList;

public class MonthsActivity extends AppCompatActivity {
   RecyclerView recyclerView;
   MothsAdpter adpter;
   LinearLayout janlv;
   Toolbar tt;
   ImageView imageView;
    TextView resulationtext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_months);

        janlv=findViewById(R.id.janlv);
        tt=findViewById(R.id.tt);
        resulationtext=tt.findViewById(R.id.resulationtext);
        //resulationtext.setVisibility(View.GONE);
        resulationtext.setText(R.string.hindi);
       // setSupportActionBar(tt);
        imageView=tt.findViewById(R.id.back);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

//        MyListData[] myListData = new MyListData[] {
//                new MyListData("जनवरी","https://patnayellowpages.com/wp-content/uploads/2020/06/January-Thakur-Prasad-Calen-1.jpg"),
//                new MyListData("फ़रवरी","https://patnayellowpages.com/wp-content/uploads/2020/06/February-2020-Thakur-Prasad-Calendar.jpg"),
//                new MyListData("मार्च","https://patnayellowpages.com/wp-content/uploads/2020/06/March-Thakur-Prasad-Calendar.jpg"),
//                new MyListData("अप्रैल","https://patnayellowpages.com/wp-content/uploads/2020/06/April-Thakur-Prasad-Calendar.jpg"),
//                new MyListData("मई","https://patnayellowpages.com/wp-content/uploads/2020/06/May-Thakur-Prasad-Calendar-1.jpg"),
//                new MyListData("जून","https://patnayellowpages.com/wp-content/uploads/2020/06/June-Thakur-Prasad-Calendar-1.jpg"),
//                new MyListData("जुलाई","https://patnayellowpages.com/wp-content/uploads/2020/06/July-Thakur-Prasad-Calendar-1.jpg"),
//                new MyListData("अगस्त","https://patnayellowpages.com/wp-content/uploads/2020/06/August-Thakur-Prasad-Calend-1.jpg"),
//                new MyListData("सितंबर","https://patnayellowpages.com/wp-content/uploads/2020/06/September-Thakur-Prasad-Cal-1.jpg"),
//                new MyListData("अक्टूबर","https://patnayellowpages.com/wp-content/uploads/2020/06/October-Thakur-Prasad-Calen-1.jpg"),
//                new MyListData("नवंबर","https://patnayellowpages.com/wp-content/uploads/2020/06/November-Thakur-Prasad-Cale-1.jpg"),
//                new MyListData("दिसंबर","https://hindijaankaari.in/wp-content/uploads/2018/12/Thakur-prasad-calendar-2020-december.jpg"),
//
//        };
        MyListData[] myListData = new MyListData[] {
                new MyListData("जनवरी","https://smarteduguide.com/wp-content/uploads/2020/11/january-2021-768x967.jpg"),
                new MyListData("फ़रवरी","https://smarteduguide.com/wp-content/uploads/2020/11/February-2021-768x979.jpg"),
                new MyListData("मार्च","https://smarteduguide.com/wp-content/uploads/2020/11/March-2021-768x976.jpg"),
                new MyListData("अप्रैल","https://smarteduguide.com/wp-content/uploads/2020/11/April-2021-768x976.jpg"),
                new MyListData("मई","https://smarteduguide.com/wp-content/uploads/2020/11/May-2021-768x976.jpg"),
                new MyListData("जून","https://smarteduguide.com/wp-content/uploads/2020/11/June-2021-768x976.jpg"),
                new MyListData("जुलाई","https://smarteduguide.com/wp-content/uploads/2020/11/July-2021-768x976.jpg"),
                new MyListData("अगस्त","https://smarteduguide.com/wp-content/uploads/2020/11/August-2021-768x976.jpg"),
                new MyListData("सितंबर","https://smarteduguide.com/wp-content/uploads/2020/11/September-2021-768x976.jpg"),
                new MyListData("अक्टूबर","https://smarteduguide.com/wp-content/uploads/2020/11/October-2021-768x976.jpg"),
                new MyListData("नवंबर","https://smarteduguide.com/wp-content/uploads/2020/11/November-2021-768x976.jpg"),
                new MyListData("दिसंबर","https://smarteduguide.com/wp-content/uploads/2020/11/Decemberr-2021-768x976.jpg")
        };

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler);
        MothsAdpter adapter = new MothsAdpter(this,myListData);
        recyclerView.setHasFixedSize(true);
       // recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setLayoutManager(new GridLayoutManager(this,3));
        recyclerView.setAdapter(adapter);


    }
}