package com.hindi.calander.panchang;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.viewpager.widget.ViewPager;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.HomeFragment;
import com.TyoharFragment;
import com.ViewPagerAdpter;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;

import java.net.URL;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{
    Toolbar toolbar;
    DrawerLayout drawer;
    ActionBarDrawerToggle actionBarDrawerToggle;
    NavigationView navigationView;
    ActionBar actionBar;
   ImageView back;
    private ViewPager viewPager;
    ViewPagerAdpter adpter;
    private TabLayout tabLayout;
    AdView adView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar=findViewById(R.id.tool);
        viewPager = findViewById(R.id.viewpager);
        tabLayout = findViewById(R.id.tabs);
        back = toolbar.findViewById(R.id.back);
         drawer = findViewById(R.id.drlayout);
         back.setVisibility(View.GONE);
        adView = findViewById(R.id.adview);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);


        navigationView = (NavigationView)findViewById(R.id.nv);
        initNavigationDrawer();
        tabLayout.addTab(tabLayout.newTab().setText(R.string.Home));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.calender));
        tabLayout.addTab(tabLayout.newTab().setText("राशिफल"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
         adpter = new ViewPagerAdpter(this,getSupportFragmentManager(),
                 tabLayout.getTabCount());
        viewPager.setAdapter(adpter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                viewPager.setCurrentItem(tab.getPosition());

            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });



    }







    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // The action bar home/up action should open or close the drawer.
//        switch (item.getItemId()) {
//            case android.R.id.home:
//                drawer.openDrawer(GravityCompat.START);
//                return true;
//        }

        if (actionBarDrawerToggle.onOptionsItemSelected(item)){

            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.navmenu, menu);
        return true;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        return false;
    }


    public void initNavigationDrawer() {

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                int id = menuItem.getItemId();
//                if (id == R.id.nav_invite) {
//                    // Handle the camera action
//                } else if (id == R.id.track) {

//                    Intent intent=new Intent(MainActivity.this,DateActivity.class);
//                    startActivity(intent);
//                }
                if (id == R.id.share) {

                    try {
                        Intent shareIntent = new Intent(Intent.ACTION_SEND);
                        shareIntent.setType("text/plain");
                        shareIntent.putExtra(Intent.EXTRA_SUBJECT, "NewYear Resolution");
                        String shareMessage = "\nHi, Download this cool & fast performance HindiCalender App.\n";
//                    shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" +"com.appzmachine.notificationmuter"+"\n\n";
                        shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" +getPackageName()+"\n\n";
                        shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                        startActivity(Intent.createChooser(shareIntent, "choose one"));
                    } catch (Exception e) {

                    }
                } else if (id == R.id.rateus) {

                    try{
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id="+getPackageName())));
                    }
                    catch (ActivityNotFoundException e){
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id="+getPackageName())));
                    }
                } else if (id == R.id.privacy) {

                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://docs.google.com/document/d/1LpNiZyaT105eVeshuL3niy_xejjNN7q2xDZp3zqx2Q4/edit"));
                    startActivity(browserIntent);
                } else if(id==R.id.more){
                    // https://play.google.com/store/apps/developer?id=Appz+machine
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/developer?id=Appz+machine"));
                    startActivity(intent);
                }
                drawer.closeDrawers();


//                switch (id){
//                    case R.id.home:
//                        Toast.makeText(getApplicationContext(),"Home",Toast.LENGTH_SHORT).show();
//                        drawer.closeDrawers();
//                        break;
//
//
//                }
                return true;
            }
        });
        View header = navigationView.getHeaderView(0);

        drawer = findViewById(R.id.drlayout);

        actionBarDrawerToggle = new ActionBarDrawerToggle(this,drawer,toolbar,R.string.navigation_drawer_open,R.string.navigation_drawer_close){

            @Override
            public void onDrawerClosed(View v){
                super.onDrawerClosed(v);
            }

            @Override
            public void onDrawerOpened(View v) {
                super.onDrawerOpened(v);
            }
        };
        drawer.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

}