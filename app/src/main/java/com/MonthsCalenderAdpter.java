package com;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.hindi.calander.panchang.R;

import java.util.Calendar;

public class MonthsCalenderAdpter extends RecyclerView.Adapter<MonthsCalenderAdpter.ViewHolder> {
    ImageView calenderimg, timer;
    EditText edttime, edtcalender, edttxt;
    Button save;
    Calendar calNow;
    int notificationid = 100;
    Context context;
    long time;
    SQLiteDatabase db;
    // RecyclerView recyclerView;
    String goaltask, calenderdate;
    String caltime;
    private MyListData[] listdata;

    public MonthsCalenderAdpter(Context context, MyListData[] listdata) {
        this.context = context;
        this.listdata = listdata;
    }


    @NonNull
    @Override
    public MonthsCalenderAdpter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.itemlayout, parent, false);
        MonthsCalenderAdpter.ViewHolder viewHolder = new MonthsCalenderAdpter.ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MonthsCalenderAdpter.ViewHolder holder, int position) {
        final MyListData myListData = listdata[position];
        holder.itemstext.setText(myListData.getDescription());
        holder.janlv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://patnayellowpages.com/wp-content/uploads/2020/06/January-Thakur-Prasad-Calen-1.jpg")));

                Intent intent = new Intent(context, Calender2021Activity.class);
                intent.putExtra("january",myListData.getUrl());
                context.startActivity(intent);


            }
        });
    }

    @Override
    public int getItemCount() {
        return listdata.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView imageView;
        public TextView itemstext;
        public LinearLayout janlv;

        public ViewHolder(View itemView) {
            super(itemView);

            itemstext = itemView.findViewById(R.id.itemstext);
            janlv = itemView.findViewById(R.id.janlv);

        }
    }
}