package com;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar ;

import com.hindi.calander.panchang.R;

public class FestivalActivity extends AppCompatActivity {
 Toolbar toolbar;
 TextView resulationtext;
 ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_festival);

        toolbar=findViewById(R.id.toolbar);
        resulationtext=toolbar.findViewById(R.id.resulationtext);
        // resulationtext.setVisibility(View.GONE);
        resulationtext.setText(R.string.hindi);
        AddsClass addsClass=new AddsClass(getApplicationContext());
        addsClass.showInterstitial();


        // toolbar.setTitle("िंदी कैलेंडर 2020");
        // setSupportActionBar(toolbar);

        imageView=findViewById(R.id.back);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}