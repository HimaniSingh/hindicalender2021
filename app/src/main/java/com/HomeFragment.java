package com;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hindi.calander.panchang.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class HomeFragment extends Fragment {
   TextView textView,calender,day;
   LinearLayout calenderlv,horoscopelv,marriagelv,festivallv,secondcallv,calenderhomegochar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_home, container, false);
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.YYYY");
        String currentDateandTime = sdf.format(new Date());
        textView=view.findViewById(R.id.currentdate);
        calender=view.findViewById(R.id.calender);
        calenderlv=view.findViewById(R.id.calenderlv);
        festivallv=view.findViewById(R.id.festivallv);
        horoscopelv=view.findViewById(R.id.horoscopelv);
        marriagelv=view.findViewById(R.id.marriagelv);
        marriagelv=view.findViewById(R.id.marriagelv);
        secondcallv=view.findViewById(R.id.secondcallv);
        day=view.findViewById(R.id.day);
        calenderhomegochar=view.findViewById(R.id.calenderhomegochar);
        textView.setText(currentDateandTime);


        String weekday_name = new SimpleDateFormat("EEEE",Locale.ENGLISH).format(System.currentTimeMillis());
        Log.i("jhgfd",weekday_name);
        day.setText(weekday_name);
//

        calenderlv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getContext(),MonthsActivity.class);
                startActivity(intent);
            }
        });
        horoscopelv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getContext(),Rashifal2021Activity.class);
                startActivity(intent);
            }
        });
        marriagelv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getContext(),Marriage2021Activity.class);
                startActivity(intent);
            }
        });
        festivallv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getContext(),Festival2021Activity.class);
                startActivity(intent);
            }
        });
        secondcallv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getContext(),Festival2021Activity.class);
                startActivity(intent);
            }
        });
        calenderhomegochar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getContext(),Marriage2021Activity.class);
                startActivity(intent);
            }
        });


        return view;
    }
}